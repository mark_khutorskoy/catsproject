﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CatsApp.cats.PlushCat;
using CatsApp.cats.FlyingCat;
using CatsApp.cats.MetalCat;
using CatsApp.cats.WoodenCat;

namespace CatsApp
{

    class Program
    {
        static void Main(string[] args)
        {      
            PlushCat plushCat = new PlushCat(" плюшевый ");
            plushCat.Meow();
            Console.WriteLine();           

            MetalCat metalCat = new MetalCat(" металлический ");
            metalCat.Meow();
            metalCat.Jump();
            Console.WriteLine();

            WoodenCat woodenCat = new WoodenCat(" деревянный ");
            woodenCat.Stay();
            Console.WriteLine();

            FlyingCat flyingCat = new FlyingCat(" летающий ");
            flyingCat.Meow();
            flyingCat.Jump();
            flyingCat.Fly();

            Console.ReadKey();
        }
    }
}
