﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatsApp.cats.MetalCat
{
    public class MetalCat : ToyCats,IMeow
    {
        public MetalCat(string phrase):base(phrase)
        {

        }
        /// умеет мяукать и прыгать
        public void Jump()
        {
            Console.Write("- Прыг-скок!");
        }

        public void Meow()
        {
            Console.Write("- МИЯУ-МИЯУ!"); 
        }
    }
}
