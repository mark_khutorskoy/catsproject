﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatsApp.cats.PlushCat
{
    public class PlushCat : ToyCats, IMeow
    {
        public PlushCat(string phrase):base(phrase)
        {
        
        }


        /// умеет мяукать
        public void Meow()
        {
            Console.Write("- МЯЯЯЯЯЯЯЯУУУ!");
        }
    }

}
