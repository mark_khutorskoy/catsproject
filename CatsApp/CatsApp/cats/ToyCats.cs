﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatsApp.cats
{
    public abstract class ToyCats
    {
        private string phrase { get; set; }

        public ToyCats(string phrase)
        {
            this.phrase = phrase;
        }

        public void Display()
        {
            Console.Write("Я" + phrase + "котик и я хочу: ");
        }

        public void Stay()
        {
            Console.Write("- только стоять");
        }
    }
}
